// Quay giải thưởng 1 người
function gift_1th_loading() {
    
    if (confirm("Bạn muốn quay số?") == true) {
        myVar = setTimeout(gift_1th, 5000);
        document.getElementById("loaderId").classList.add("loader");
    } else {
        return;
    }
}

function gift_1th() {
    var randomItem = winnerbTaskee[Math.floor(Math.random()*winnerbTaskee.length)];
  
    // document.getElementById("current").innerHTML = "";
    
    // In ra 1 người chiến thắng
    document.getElementById("lucky_1th").innerHTML = randomItem.code;
    document.getElementById("name_1th").innerHTML = randomItem.name;
    document.getElementById("phone_1th").innerHTML = randomItem.phone;
  
    // Xoá các phần tử trùng
    for( var i = 0; i < winnerbTaskee.length; i++){ 
       if (winnerbTaskee[i].userId === randomItem.userId) {
         winnerbTaskee.splice(i, 1); 
       }
    }
    document.getElementById("loaderId").classList.remove("loader");
    // Số lượng còn lại
    // document.getElementById("current").innerHTML = winnerbTaskee.length;
}




// Tìm 70 người trúng thưởng giải 4
function gift_4th_loading() {
    
    if (confirm("Bạn muốn quay số?") == true) {
        myVar = setTimeout(gift_4th, 5000);
        document.getElementById("loaderId").classList.add("loader");
    } else {
        return;
    }
}

function gift_4th() {
  for (var t = 1; t <= 70; t++) {

    // In ra lần lượt người đạt giải
    var randomItem = winnerbTaskee[Math.floor(Math.random()*winnerbTaskee.length)];
    var node = document.createElement("LI");
    var textnode = document.createTextNode(t + ": " + randomItem.code + " - " + randomItem.name + " - " + randomItem.phone);
    node.appendChild(textnode);
    document.getElementById("gift4th").appendChild(node);

    // Xoá các phần tử trùng
    for( var j = 0; j < winnerbTaskee.length; j++){ 
       if ( winnerbTaskee[j].userId === randomItem.userId) {
         winnerbTaskee.splice(j, 1); 
       }
    }
  }
  document.getElementById("loaderId").classList.remove("loader");
  // Số lượng còn lại
//   document.getElementById("current").innerHTML = winnerbTaskee.length;
}
